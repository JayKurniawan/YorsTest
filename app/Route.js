import SplashPage from './page/SplashPage';
import ContactListPage from './page/ContactListPage';
import ContactDetail from './page/ContactDetail';

var conf = Navigator.SceneConfigs.FloatFromBottom;
conf.gestures = null;


import {
    Navigator,
} from 'react-native';

let navigator;

const routeMap = new Map();
routeMap.set('SplashPage', {
    component: SplashPage
});

routeMap.set('ContactListPage', {
    component: ContactListPage
});

routeMap.set('ContactDetail', {
    component: ContactDetail
});

export function registerNavigator(tempNavigator) {
    if (navigator) {
        return;
    }
    navigator = tempNavigator;

}

export function getNavigator() {
    return navigator;
}

export function setCurrentNavigator(name) {
    let nagivators = navigator.getCurrentRoutes();
    for (let i = 0; i < nagivators.length; i++) {
        if (nagivators[i].name == name) {
            nagivators.splice(i - 1, nagivators.length);
            navigator.setCurrentRoutes(nagivators);

            navigator.popToRoute(nagivators[i]);
        }
    }

    let nagivators2 = navigator.getCurrentRoutes();
}

export function getRouteMap() {
    return routeMap;
}
