/**
 * Created by lipeiwei on 16/10/2.
 */

import {StyleSheet, Dimensions} from 'react-native';
export const windowWidth = Dimensions.get('window').width;
export const windowHeight = Dimensions.get('window').height;

const colors = {
    MAIN_COLOR: 'rgb(0,0,0)',//E7646E
    GREEN_COLOR: '#00D1C1',
    LINE_GRAY_COLOR: '#E8E8E8',
    GRAY_COLOR: '#DADADA',
    TEXT_GRAY_COLOR: '#79767C',
    TEXT_COLOR: '#443046',
    BACKGROUND_COLOR: '#F7F7F7',
    BACKGROUND_GRAY: '#EEEEEE',
    LIGHT_BLUE_COLOR: '#5CACEE',//浅蓝色
    LIGHT_GRAY_COLOR: '#cccccc44',//浅灰色
    WIDTH: windowWidth / 400, //设计默认为iphone7 Plus，5.5寸屏幕，414*736
    HEIGHT: windowHeight / 800,
};

const size = {
    BUTTON_TEXT: 16 * colors.WIDTH,
    TIP_TEXT: 10 * colors.WIDTH,
    INPUT_TEXT: 12 * colors.WIDTH,

}


export default {
    ...colors,
    ...size,
    HORIZONTAL_SPACE: 10,
    windowWidth,
    windowHeight,
    tiptext: {
        color: colors.GREEN_COLOR,
        fontSize: 10 * colors.WIDTH,
        fontWeight: 'bold',
        marginTop: 10 * colors.HEIGHT,
    },
    sendverificbtn: {
        height: 30 * colors.HEIGHT,
        width: 100 * colors.WIDTH,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 3 * colors.WIDTH,
        borderRadius: 5 * colors.WIDTH,
        borderWidth:1* colors.HEIGHT,
        borderColor:colors.GREEN_COLOR
    },
};
