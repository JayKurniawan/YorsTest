import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Modal,
    TouchableHighlight,
    TouchableOpacity,
    Image,
    Dimensions,
    Animated,
    ActivityIndicator,
    ProgressBarAndroid,
    ActivityIndicatorIOS,
    Platform,
    ToastAndroid,
    StatusBarIOS,
    StatusBar,
    ScrollView
} from 'react-native';
import { SearchBar } from 'react-native-elements';
import commonStyle from '../style/commonStyle';
import ContactListPage from './ContactListPage';
import ContactDetail from './ContactDetail';
import Fetch from '../Fetch';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: commonStyle.BACKGROUND_COLOR,
    },
    gifcontainer: {
        width: commonStyle.windowWidth,
        height: commonStyle.windowHeight,
        alignItems: 'center',
        justifyContent:'center',
        position: 'absolute',
    },
    imgtainer: {
        width: 90 * commonStyle.WIDTH,
        height: 90 * commonStyle.HEIGHT,
        padding: 20 * commonStyle.WIDTH,
        borderRadius: 10 * commonStyle.WIDTH,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#00000099'
    },
    imgstyle: {
        width: 25 * commonStyle.WIDTH,
        height: 25 * commonStyle.HEIGHT,
    },

    toastcontain: {
        position: 'absolute',
        bottom: 150 * commonStyle.HEIGHT,
        width: commonStyle.windowWidth,
        alignItems: 'center',
        justifyContent: 'center',
    },

    txtcontain: {
        backgroundColor: '#00000099',
        paddingVertical: 10 * commonStyle.WIDTH,
        paddingHorizontal: 20 * commonStyle.WIDTH,
        borderRadius: 5,
    },
    toasttxt: {
        fontSize: 16 * commonStyle.WIDTH,
        color: 'white'
    },head:{
        backgroundColor: '#badc58',
        height: 60,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textHead:{
        paddingLeft: 10,
        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold'
    }
});

class BasePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loadingVisible: false,
            toastVisible: false,
            contacts: []
        };
        this.renderBody = this.renderBody.bind(this);
    }

    componentWillUnmount() {
        this.setLoadingVisible(false);
        clearTimeout(this.timer);
    }


    componentDidMount() {
        
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderHeader()}
                {this.renderBody()}
                {this.renderFooter()}
                {this.state.loadingVisible &&
                <View style={styles.gifcontainer}>
                    <View style={styles.imgtainer}>
                        {this._renderActivityIndicator()}
                    </View>
                </View>
                }

                {this.state.toastVisible &&
                <View style={styles.toastcontain}>
                    <View style={styles.txtcontain}>
                        <Text style={styles.toasttxt}>{this.state.content}</Text>
                    </View>
                </View>
                }
            </View>
        );
    }

    _renderActivityIndicator() {
        return ActivityIndicator ? (
            <ActivityIndicator
                animating={true}
                color={'white'}
                size={'small'}/>
        ) : Platform.OS == 'android' ?
            (
                <ProgressBarAndroid
                    style={{marginRight: 10 * commonStyle.WIDTH,}}
                    color={'white'}
                    styleAttr={'Small'}/>

            ) : (
                <ActivityIndicatorIOS
                    style={{marginRight: 10 * commonStyle.WIDTH,}}
                    animating={true}
                    color={'white'}
                    size={'small'}/>
            )
    }


    setLoadingVisible(visible) {
        this.setState({
            loadingVisible: visible
        });
    }

    toastShort(content) {
        if (Platform.OS === 'ios') {
            this.setState({toastVisible: true, content})
            this.timer = setTimeout(() => {
                this.setState({toastVisible: false});
            }, 2000)
        } else {
            ToastAndroid.show(content.toString(), ToastAndroid.SHORT);
        }
    }

    //Override Func goes here

    renderBody() 
    {
        return(
            <ContactListPage/>
            // <ContactDetail/>
        )  
    }

    renderHeader()
    {
        
        return(
            <View>
                <View style={styles.head}>
                    <Text style={styles.textHead}>My Contacts</Text>
                </View>
                <SearchBar
                    onChangeText={(data)=>this.searchContact(data)}
                    placeholder='Find contact' 
                />
            </View>
        )  
    }
    
    renderFooter()
    {

    }
}

export default BasePage;
