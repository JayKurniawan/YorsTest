import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import {connect} from 'react-redux';
import {getNavigator} from '../Route';
import {home} from '../action/splash';

class ContactDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View>
                    <View style={styles.imgWrapper}>
                        <Image style={styles.image} source={{uri: 'https://buffer-uploads.s3.amazonaws.com/503a5c8ffc99f72a7f00002e/f49c2ff693f1c307af5e1b3d84e581ca.png'}}/>
                    </View>
                </View>
                <View>
                    <View>
                        <Text style={styles.name}>Name</Text>
                        <Text style={styles.detail}>Phone</Text>
                        <Text style={styles.detail}>Description</Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    image:{
        width: 200,
        height: 200,
        borderWidth: 5,
        borderColor: 'white',
        borderRadius: 100
    },
    imgWrapper:{
        alignItems: 'center',
        marginTop: -170
    },
    name:{
        marginTop: 5,
        fontSize: 40
    },
    detail:{
        marginTop: 5
    }
});

function mapStateToProps(state) {
    const {home} = state;
    return {
        home
    }
}

export default connect(mapStateToProps)(ContactDetail);