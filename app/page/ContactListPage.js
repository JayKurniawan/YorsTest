import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Image } from 'react-native';
import { home } from '../action/splash';
import { connect } from 'react-redux';

// create a component
class ContactListPage extends Component {

    constructor(props){
        super(props);
        this.state = {
            data: []
        }
    }

    componentWillMount(){
        this.props.home
    }

    render() {
        return (
            <ScrollView>
                {this.fetchContact()}
                {this.fetchContact()}
                {this.fetchContact()}
                {this.fetchContact()}
                {this.fetchContact()}
                {this.fetchContact()}
                {this.fetchContact()}
                {this.fetchContact()}
                {this.fetchContact()}
            </ScrollView>
        );
    }

    fetchContact(contact){
        return(
            <TouchableOpacity style={styles.body}>
                <Image style={styles.thumbnail} source={{uri: 'https://buffer-uploads.s3.amazonaws.com/503a5c8ffc99f72a7f00002e/f49c2ff693f1c307af5e1b3d84e581ca.png'}}/>
                <View style={styles.nameContainer}>
                    <Text style={styles.name}>Contact Name</Text>
                </View>
            </TouchableOpacity>
        )
    }
    
}

// define your styles
const styles = StyleSheet.create({
    body:{
        padding: 10,
        flexDirection: 'row',
        borderBottomWidth: 1
    },
    thumbnail:{
        width: 50,
        height: 50,
        marginRight: 15,
        borderRadius: 10
    },
    name:{
        fontSize: 25
    },
    nameContainer:{
        justifyContent: 'center'
    }
});

function mapStateToProps(state) {
    const {home} = state;
    return {
        home
    }
}

export default connect(mapStateToProps)(ContactListPage);
